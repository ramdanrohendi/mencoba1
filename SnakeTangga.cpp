#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>

using namespace std;

void setcolor(unsigned short color) { //Warna Tampilan
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hCon,color);
}

void gotoxy(int x, int y) { //Koordinat
	COORD k = {x,y};
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),k);
}

void delay(int a) { //delay atau jeda waktu
	for(int x=0; x<a*100; x++)
	{
		for(int y=0; y<a*100;y++);
	}
}

int acak;

int dadu(){
    srand( time(0) );
    acak = ( rand()%6 + 1 );
    return acak;
}

int posisi = 0;
int posisi2 = 0;
int posisi3 = 0;
int posisi4 = 0;

string pemain1;
string pemain2;
string pemain3;
string pemain4;

main(){
    char bermain;
    int jumPem;
    
    getch();
    gotoxy(13,0); cout << " ";
    delay(85);
    system("Color A0"); gotoxy(14,0); cout << "T";
    delay(85);
    system("Color B1"); gotoxy(12,0); cout << "E";
    delay(85);
    system("Color C2"); gotoxy(15,0); cout << "A";
    delay(85);
    system("Color D3"); gotoxy(11,0); cout << "K";
    delay(85);
    system("Color E4"); gotoxy(16,0); cout << "N";
    delay(85);
    system("Color F5"); gotoxy(10,0); cout << "A";
    delay(85);
    system("Color A6"); gotoxy(17,0); cout << "G";
    delay(85);
    system("Color B7"); gotoxy(9,0); cout << "N";
    delay(85);
    system("Color C8"); gotoxy(18,0); cout << "G";
    delay(85);
    system("Color D9"); gotoxy(8,0); cout << "S";
    delay(85);
    system("Color E0"); gotoxy(19,0); cout << "A";
    for(int a = 7, b = 20; a >= 0, b <= 27; a--, b++){
		delay(50);
	    system("Color 70"); gotoxy(a,0); cout << "<";
	    delay(50); 
	    system("Color 0A"); gotoxy(b,0); cout << ">";
    }
    system("Color 70");
    cout << endl;
    cout << "       \"Beta Version\"       " << endl;
    delay(100);
    cout << endl;
    cout << "Mulai ( y/t ) ? ";
    cin >> bermain;
    if( bermain == 'y' || bermain == 'Y' ){
		cout << "Jumlah Pemain ( 2-4 ) : ";
  		cin >> jumPem;
  		if( jumPem == 1 ){
			cout << "Nama pemain 1 : ";
			cin >> pemain1;
			cout << endl;
			cout << "Mulai Permainan ( y )";
			getch();
	    	do{
				system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
    			cout << endl;
    			cout << "Peta : " << endl;
    			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0){
	    	        cout << "  - Posisi : " << pemain1 << " = START" << endl;
	    	    }
	    	    else {
    				cout << "  - Posisi : " << pemain1 << " = " << posisi << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain1 << endl;
	    	    if(posisi == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi << endl;
	    	    }
	    	    cout << "  - Roll Dadu : " << endl;
	    	    getch();
	    	    cout << "  - Dadu : " << dadu() << endl;
	    	    posisi += dadu();
	    	    //posisi menang
	    	    if( posisi == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain1 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
	    	        getch();
	    	        break;
	    	    }
	    	    //tangga
	    	    else if( posisi == 2 ){
	    	        posisi = 38;
	    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
	    	    }
	    	    else if( posisi == 7 ){
	    	        posisi = 53;
	    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
	    	    }
	    	    else if( posisi == 19 ){
	    	        posisi = 80;
	    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
	    	    }
	    	    else if( posisi == 36 ){
	    	        posisi = 66;
	    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
	    	    }
	    	    else if( posisi == 74 ){
	    	        posisi = 93;
	    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
	    	    }
	    	    //ular
	    	    else if( posisi == 42 ){
	    	        posisi = 5;
	    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
	    	    }
	    	    else if( posisi == 71 ){
	    	        posisi = 33;
	    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
	    	    }
	    	    else if( posisi == 77 ){
	    	        posisi = 56;
	    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
	    	    }
	    	    else if( posisi == 86 ){
	    	        posisi = 24;
	    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
	    	    }
	    	    else if( posisi == 98 ){
	    	        posisi = 3;
	   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
	   		    }
	   	    	//bila melebihi jumlah petak
	   	    	else if( posisi > 100 ){
	        	    int sisa = posisi;
	        	    sisa -= 100;
	        	    posisi -= sisa;
	        	    posisi -= sisa;
	        	    if( posisi == 98 ){
	        	        posisi = 3;
	        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
	        	    }
	        	}
	    	    //angka dadu 6 pertama
	    	    if( dadu() == 6 ){
	    	        cout << "    Anda roll dadu lagi :)" << endl;
	    	        if(posisi == 0){
	    	    	    cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi << endl;
		    	    }
		    	    cout << "  - Roll Dadu : " << endl;
		    	    getch();
		    	    cout << "  - Dadu : " << dadu() << endl;
		    	    posisi += dadu();
		    	    //posisi menang
		    	    if( posisi == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain1 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
		    	        getch();
						break;
		    	    }
		    	    //tangga
		    	    else if( posisi == 2 ){
		    	        posisi = 38;
		    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 7 ){
		    	        posisi = 53;
		    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 19 ){
		    	        posisi = 80;
		    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 36 ){
		    	        posisi = 66;
		    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 74 ){
		    	        posisi = 93;
		    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		    	    }
		    	    //ular
		    	    else if( posisi == 42 ){
		    	        posisi = 5;
		    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 71 ){
		    	        posisi = 33;
		    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 77 ){
		    	        posisi = 56;
		    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 86 ){
		    	        posisi = 24;
		    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 98 ){
		    	        posisi = 3;
		   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
		   		    }
		   	    	//bila melebihi jumlah petak
		   	    	else if( posisi > 100 ){
		        	    int sisa = posisi;
		        	    sisa -= 100;
		        	    posisi -= sisa;
		        	    posisi -= sisa;
		        	    if( posisi == 98 ){
		        	        posisi = 3;
		        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		        	    }
		        	}
	  	        	//angka dadu 6 kedua
	  	    		if( dadu() == 6 ){
	  	    	    	cout << "    Anda main lagi :)" << endl;
	        	        if(posisi == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi << endl;
			    	    }
			    	    cout << "  - Roll Dadu : " << endl;
			    	    getch();
			    	    cout << "  - Dadu : " << dadu() << endl;
			    	    posisi += dadu();
			    	    //posisi menang
			    	    if( posisi == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain1 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
			    	        getch();
							break;
			    	    }
			    	    //tangga
			    	    else if( posisi == 2 ){
			    	        posisi = 38;
			    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 7 ){
			    	        posisi = 53;
			    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 19 ){
			    	        posisi = 80;
			    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 36 ){
			    	        posisi = 66;
			    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 74 ){
			    	        posisi = 93;
			    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			    	    }
			    	    //ular
			    	    else if( posisi == 42 ){
			    	        posisi = 5;
			    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 71 ){
			    	        posisi = 33;
			    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 77 ){
			    	        posisi = 56;
			    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 86 ){
			    	        posisi = 24;
			    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 98 ){
			    	        posisi = 3;
			   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
			   		    }
			   	    	//bila melebihi jumlah petak
			   	    	else if( posisi > 100 ){
			        	    int sisa = posisi;
			        	    sisa -= 100;
			        	    posisi -= sisa;
			        	    posisi -= sisa;
			        	    if( posisi == 98 ){
			        	        posisi = 3;
			        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			        	    }
			        	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi = 0;
		                }
		            }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        cout << endl;
			}while( true );
    	}
    	else if( jumPem == 2 ) {
			cout << "Nama pemain 1 : ";
			cin >> pemain1;
			cout << "Nama pemain 2 : ";
			cin >> pemain2;
			cout << endl;
			cout << "Mulai Permainan ( y )";
			getch();
			do{
				system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
    			}
	    		else if(posisi == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	    }
	    	    else if(posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain1 << endl;
	    	    if(posisi == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi += dadu();
		   	    //posisi menang
		   	    if( posisi == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain1 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi == 2 ){
		   	        posisi = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 7 ){
		   	        posisi = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 19 ){
		   	        posisi = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 36 ){
		   	        posisi = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 74 ){
		   	        posisi = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi == 42 ){
		   	        posisi = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 71 ){
		   	        posisi = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 77 ){
		   	        posisi = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 86 ){
		   	        posisi = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi == 98 ){
		   	        posisi = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi > 100 ){
		       	    int sisa = posisi;
		       	    sisa -= 100;
		       	    posisi -= sisa;
		       	    posisi -= sisa;
		       	    if( posisi == 98 ){
		       	        posisi = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi == 0){
		   	    	    cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi << endl;
		    	    }
		    	    cout << "  - Roll Dadu : " << endl;
		    	    getch();
		    	    cout << "  - Dadu : " << dadu() << endl;
		    	    posisi += dadu();
		    	    //posisi menang
		    	    if( posisi == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain1 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
		    	    	getch();
					    break;
		    	    }
			   	    //tangga
			   	    else if( posisi == 2 ){
			   	        posisi = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 7 ){
		    	        posisi = 53;
		    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 19 ){
		    	        posisi = 80;
		    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 36 ){
		    	        posisi = 66;
		    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 74 ){
		    	        posisi = 93;
		    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		    	    }
		    	    //ular
		    	    else if( posisi == 42 ){
		    	        posisi = 5;
		    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 71 ){
		    	        posisi = 33;
		    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 77 ){
		    	        posisi = 56;
		    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 86 ){
		    	        posisi = 24;
		    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 98 ){
		    	        posisi = 3;
		   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
		   		    }
		   	    	//bila melebihi jumlah petak
		   	    	else if( posisi > 100 ){
		        	    int sisa = posisi;
		        	    sisa -= 100;
		        	    posisi -= sisa;
		        	    posisi -= sisa;
		        	    if( posisi == 98 ){
		        	        posisi = 3;
		        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		        	    }
		        	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi << endl;
			    	    }
			    	    cout << "  - Roll Dadu : " << endl;
			    	    getch();
			    	    cout << "  - Dadu : " << dadu() << endl;
			    	    posisi += dadu();
			    	    //posisi menang
			    	    if( posisi == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain1 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
			    	        getch();
							break;
			    	    }
			    	    //tangga
			    	    else if( posisi == 2 ){
			    	        posisi = 38;
			    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 7 ){
			    	        posisi = 53;
			    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 19 ){
			    	        posisi = 80;
			    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 36 ){
			    	        posisi = 66;
			    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 74 ){
			    	        posisi = 93;
			    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			    	    }
			    	    //ular
			    	    else if( posisi == 42 ){
			    	        posisi = 5;
			    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 71 ){
			    	        posisi = 33;
			    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 77 ){
			    	        posisi = 56;
			    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 86 ){
			    	        posisi = 24;
			    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 98 ){
			    	        posisi = 3;
			   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
			   		    }
			   	    	//bila melebihi jumlah petak
			   	    	else if( posisi > 100 ){
			        	    int sisa = posisi;
			        	    sisa -= 100;
			        	    posisi -= sisa;
			        	    posisi -= sisa;
			        	    if( posisi == 98 ){
			        	        posisi = 3;
			        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			        	    }
			        	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
    			}
	    		else if(posisi == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	    }
	    	    else if(posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain2 << endl;
	    	    if(posisi2 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi2 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi2 += dadu();
		   	    //posisi menang
		   	    if( posisi2 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain2 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi2 == 2 ){
		   	        posisi2 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 7 ){
		   	        posisi2 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 19 ){
		   	        posisi2 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 36 ){
		   	        posisi2 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 74 ){
		   	        posisi2 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi2 == 42 ){
		   	        posisi2 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 71 ){
		   	        posisi2 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 77 ){
		   	        posisi2 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 86 ){
		   	        posisi2 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi2 == 98 ){
		   	        posisi2 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi2 > 100 ){
		       	    int sisa = posisi2;
		       	    sisa -= 100;
		       	    posisi2 -= sisa;
		       	    posisi2 -= sisa;
		       	    if( posisi2 == 98 ){
		       	        posisi2 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi2 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi2 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi2 += dadu();
			   	    //posisi menang
			   	    if( posisi2 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain2 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	    	getch();
					    break;
			   	    }
			   	    //tangga
			   	    else if( posisi2 == 2 ){
			   	        posisi2 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 7 ){
			   	        posisi2 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 19 ){
			   	        posisi2 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 36 ){
			   	        posisi2 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 74 ){
			   	        posisi2 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi2 == 42 ){
			   	        posisi2 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 71 ){
			   	        posisi2 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 77 ){
			   	        posisi2 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 86 ){
			   	        posisi2 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi2 == 98 ){
			   	        posisi2 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi2 > 100 ){
			       	    int sisa = posisi2;
			       	    sisa -= 100;
			       	    posisi2 -= sisa;
			       	    posisi2 -= sisa;
			       	    if( posisi2 == 98 ){
			       	        posisi2 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi2 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi2 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi2 += dadu();
				   	    //posisi menang
				   	    if( posisi2 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain2 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi2 == 2 ){
				   	        posisi2 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 7 ){
				   	        posisi2 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 19 ){
				   	        posisi2 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 36 ){
				   	        posisi2 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 74 ){
				   	        posisi2 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi2 == 42 ){
				   	        posisi2 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 71 ){
				   	        posisi2 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 77 ){
				   	        posisi2 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 86 ){
				   	        posisi2 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi2 == 98 ){
				   	        posisi2 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi2 > 100 ){
				       	    int sisa = posisi2;
				       	    sisa -= 100;
				       	    posisi2 -= sisa;
				       	    posisi2 -= sisa;
				       	    if( posisi2 == 98 ){
				       	        posisi2 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi2 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi2 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi2 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
	        }while( true );
   		}
   		else if( jumPem == 3 ) {
			cout << "Nama pemain 1 : ";
			cin >> pemain1;
			cout << "Nama pemain 2 : ";
			cin >> pemain2;
			cout << "Nama pemain 3 : ";
			cin >> pemain3;
			cout << endl;
			cout << "Mulai Permainan ( y )";
			getch();
			do{
				system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
	    		else if(posisi == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi3 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain1 << endl;
	    	    if(posisi == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi += dadu();
		   	    //posisi menang
		   	    if( posisi == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain1 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi == 2 ){
		   	        posisi = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 7 ){
		   	        posisi = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 19 ){
		   	        posisi = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 36 ){
		   	        posisi = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 74 ){
		   	        posisi = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi == 42 ){
		   	        posisi = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 71 ){
		   	        posisi = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 77 ){
		   	        posisi = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 86 ){
		   	        posisi = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi == 98 ){
		   	        posisi = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi > 100 ){
		       	    int sisa = posisi;
		       	    sisa -= 100;
		       	    posisi -= sisa;
		       	    posisi -= sisa;
		       	    if( posisi == 98 ){
		       	        posisi = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi == 0){
		   	    	    cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi << endl;
		    	    }
		    	    cout << "  - Roll Dadu : " << endl;
		    	    getch();
		    	    cout << "  - Dadu : " << dadu() << endl;
		    	    posisi += dadu();
		    	    //posisi menang
		    	    if( posisi == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain1 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
		    	    	getch();
					    break;
		    	    }
			   	    //tangga
			   	    else if( posisi == 2 ){
			   	        posisi = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 7 ){
		    	        posisi = 53;
		    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 19 ){
		    	        posisi = 80;
		    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 36 ){
		    	        posisi = 66;
		    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 74 ){
		    	        posisi = 93;
		    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		    	    }
		    	    //ular
		    	    else if( posisi == 42 ){
		    	        posisi = 5;
		    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 71 ){
		    	        posisi = 33;
		    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 77 ){
		    	        posisi = 56;
		    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 86 ){
		    	        posisi = 24;
		    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 98 ){
		    	        posisi = 3;
		   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
		   		    }
		   	    	//bila melebihi jumlah petak
		   	    	else if( posisi > 100 ){
		        	    int sisa = posisi;
		        	    sisa -= 100;
		        	    posisi -= sisa;
		        	    posisi -= sisa;
		        	    if( posisi == 98 ){
		        	        posisi = 3;
		        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		        	    }
		        	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi << endl;
			    	    }
			    	    cout << "  - Roll Dadu : " << endl;
			    	    getch();
			    	    cout << "  - Dadu : " << dadu() << endl;
			    	    posisi += dadu();
			    	    //posisi menang
			    	    if( posisi == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain1 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
			    	        getch();
							break;
			    	    }
			    	    //tangga
			    	    else if( posisi == 2 ){
			    	        posisi = 38;
			    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 7 ){
			    	        posisi = 53;
			    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 19 ){
			    	        posisi = 80;
			    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 36 ){
			    	        posisi = 66;
			    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 74 ){
			    	        posisi = 93;
			    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			    	    }
			    	    //ular
			    	    else if( posisi == 42 ){
			    	        posisi = 5;
			    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 71 ){
			    	        posisi = 33;
			    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 77 ){
			    	        posisi = 56;
			    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 86 ){
			    	        posisi = 24;
			    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 98 ){
			    	        posisi = 3;
			   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
			   		    }
			   	    	//bila melebihi jumlah petak
			   	    	else if( posisi > 100 ){
			        	    int sisa = posisi;
			        	    sisa -= 100;
			        	    posisi -= sisa;
			        	    posisi -= sisa;
			        	    if( posisi == 98 ){
			        	        posisi = 3;
			        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			        	    }
			        	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
	    		else if(posisi == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi3 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain2 << endl;
	    	    if(posisi2 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi2 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi2 += dadu();
		   	    //posisi menang
		   	    if( posisi2 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain2 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi2 == 2 ){
		   	        posisi2 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 7 ){
		   	        posisi2 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 19 ){
		   	        posisi2 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 36 ){
		   	        posisi2 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 74 ){
		   	        posisi2 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi2 == 42 ){
		   	        posisi2 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 71 ){
		   	        posisi2 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 77 ){
		   	        posisi2 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 86 ){
		   	        posisi2 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi2 == 98 ){
		   	        posisi2 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi2 > 100 ){
		       	    int sisa = posisi2;
		       	    sisa -= 100;
		       	    posisi2 -= sisa;
		       	    posisi2 -= sisa;
		       	    if( posisi2 == 98 ){
		       	        posisi2 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi2 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi2 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi2 += dadu();
			   	    //posisi menang
			   	    if( posisi2 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain2 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	    	getch();
					    break;
			   	    }
			   	    //tangga
			   	    else if( posisi2 == 2 ){
			   	        posisi2 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 7 ){
			   	        posisi2 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 19 ){
			   	        posisi2 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 36 ){
			   	        posisi2 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 74 ){
			   	        posisi2 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi2 == 42 ){
			   	        posisi2 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 71 ){
			   	        posisi2 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 77 ){
			   	        posisi2 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 86 ){
			   	        posisi2 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi2 == 98 ){
			   	        posisi2 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi2 > 100 ){
			       	    int sisa = posisi2;
			       	    sisa -= 100;
			       	    posisi2 -= sisa;
			       	    posisi2 -= sisa;
			       	    if( posisi2 == 98 ){
			       	        posisi2 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi2 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi2 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi2 += dadu();
				   	    //posisi menang
				   	    if( posisi2 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain2 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi2 == 2 ){
				   	        posisi2 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 7 ){
				   	        posisi2 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 19 ){
				   	        posisi2 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 36 ){
				   	        posisi2 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 74 ){
				   	        posisi2 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi2 == 42 ){
				   	        posisi2 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 71 ){
				   	        posisi2 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 77 ){
				   	        posisi2 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 86 ){
				   	        posisi2 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi2 == 98 ){
				   	        posisi2 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi2 > 100 ){
				       	    int sisa = posisi2;
				       	    sisa -= 100;
				       	    posisi2 -= sisa;
				       	    posisi2 -= sisa;
				       	    if( posisi2 == 98 ){
				       	        posisi2 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi2 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi2 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi2 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
    			}
	    		else if(posisi == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	    }
	    	    else if(posisi3 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain3 << endl;
	    	    if(posisi3 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi3 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi3 += dadu();
		   	    //posisi menang
		   	    if( posisi3 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain3 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi3 == 2 ){
		   	        posisi3 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 7 ){
		   	        posisi3 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 19 ){
		   	        posisi3 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 36 ){
		   	        posisi3 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 74 ){
		   	        posisi3 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi3 == 42 ){
		   	        posisi3 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 71 ){
		   	        posisi3 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 77 ){
		   	        posisi3 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 86 ){
		   	        posisi3 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi3 == 98 ){
		   	        posisi3 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi3 > 100 ){
		       	    int sisa = posisi3;
		       	    sisa -= 100;
		       	    posisi3 -= sisa;
		       	    posisi3 -= sisa;
		       	    if( posisi3 == 98 ){
		       	        posisi3 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi3 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi3 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi3 += dadu();
			   	    //posisi menang
			   	    if( posisi3 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain3 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	        getch();
						break;
			   	    }
			   	    //tangga
			   	    else if( posisi3 == 2 ){
			   	        posisi3 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 7 ){
			   	        posisi3 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 19 ){
			   	        posisi3 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 36 ){
			   	        posisi3 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 74 ){
			   	        posisi3 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi3 == 42 ){
			   	        posisi3 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 71 ){
			   	        posisi3 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 77 ){
			   	        posisi3 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 86 ){
			   	        posisi3 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi3 == 98 ){
			   	        posisi3 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi3 > 100 ){
			       	    int sisa = posisi3;
			       	    sisa -= 100;
			       	    posisi3 -= sisa;
			       	    posisi3 -= sisa;
			       	    if( posisi3 == 98 ){
			       	        posisi3 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi3 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi3 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi3 += dadu();
				   	    //posisi menang
				   	    if( posisi3 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain3 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi3 == 2 ){
				   	        posisi3 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 7 ){
				   	        posisi3 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 19 ){
				   	        posisi3 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 36 ){
				   	        posisi3 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 74 ){
				   	        posisi3 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi3 == 42 ){
				   	        posisi3 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 71 ){
				   	        posisi3 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 77 ){
				   	        posisi3 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 86 ){
				   	        posisi3 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi3 == 98 ){
				   	        posisi3 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi3 > 100 ){
				       	    int sisa = posisi3;
				       	    sisa -= 100;
				       	    posisi3 -= sisa;
				       	    posisi3 -= sisa;
				       	    if( posisi3 == 98 ){
				       	        posisi3 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi3 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi3 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi3 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
	        }while( true );
   		}
   		else if( jumPem == 4 ) {
			cout << "Nama pemain 1 : ";
			cin >> pemain1;
			cout << "Nama pemain 2 : ";
			cin >> pemain2;
			cout << "Nama pemain 3 : ";
			cin >> pemain3;
			cout << "Nama pemain 4 : ";
			cin >> pemain4;
			cout << endl;
			cout << "Mulai Permainan ( y )";
			getch();
			do{
				system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = " << posisi4 << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
	    		else if(posisi == 0 && posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	        
	    	    }
	    	    else if(posisi3 == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else if(posisi == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
	   				cout << "             - " << pemain4 << " = " << posisi4 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain1 << endl;
	    	    if(posisi == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi += dadu();
		   	    //posisi menang
		   	    if( posisi == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain1 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi == 2 ){
		   	        posisi = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 7 ){
		   	        posisi = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 19 ){
		   	        posisi = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 36 ){
		   	        posisi = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi == 74 ){
		   	        posisi = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi == 42 ){
		   	        posisi = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 71 ){
		   	        posisi = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 77 ){
		   	        posisi = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi == 86 ){
		   	        posisi = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi == 98 ){
		   	        posisi = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi > 100 ){
		       	    int sisa = posisi;
		       	    sisa -= 100;
		       	    posisi -= sisa;
		       	    posisi -= sisa;
		       	    if( posisi == 98 ){
		       	        posisi = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi == 0){
		   	    	    cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi << endl;
		    	    }
		    	    cout << "  - Roll Dadu : " << endl;
		    	    getch();
		    	    cout << "  - Dadu : " << dadu() << endl;
		    	    posisi += dadu();
		    	    //posisi menang
		    	    if( posisi == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain1 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
		    	    	getch();
					    break;
		    	    }
			   	    //tangga
			   	    else if( posisi == 2 ){
			   	        posisi = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 7 ){
		    	        posisi = 53;
		    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 19 ){
		    	        posisi = 80;
		    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 36 ){
		    	        posisi = 66;
		    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		    	    }
		    	    else if( posisi == 74 ){
		    	        posisi = 93;
		    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		    	    }
		    	    //ular
		    	    else if( posisi == 42 ){
		    	        posisi = 5;
		    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 71 ){
		    	        posisi = 33;
		    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 77 ){
		    	        posisi = 56;
		    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 86 ){
		    	        posisi = 24;
		    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		    	    }
		    	    else if( posisi == 98 ){
		    	        posisi = 3;
		   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
		   		    }
		   	    	//bila melebihi jumlah petak
		   	    	else if( posisi > 100 ){
		        	    int sisa = posisi;
		        	    sisa -= 100;
		        	    posisi -= sisa;
		        	    posisi -= sisa;
		        	    if( posisi == 98 ){
		        	        posisi = 3;
		        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		        	    }
		        	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi << endl;
			    	    }
			    	    cout << "  - Roll Dadu : " << endl;
			    	    getch();
			    	    cout << "  - Dadu : " << dadu() << endl;
			    	    posisi += dadu();
			    	    //posisi menang
			    	    if( posisi == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain1 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
			    	        getch();
							break;
			    	    }
			    	    //tangga
			    	    else if( posisi == 2 ){
			    	        posisi = 38;
			    	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 7 ){
			    	        posisi = 53;
			    	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 19 ){
			    	        posisi = 80;
			    	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 36 ){
			    	        posisi = 66;
			    	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			    	    }
			    	    else if( posisi == 74 ){
			    	        posisi = 93;
			    	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			    	    }
			    	    //ular
			    	    else if( posisi == 42 ){
			    	        posisi = 5;
			    	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 71 ){
			    	        posisi = 33;
			    	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 77 ){
			    	        posisi = 56;
			    	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 86 ){
			    	        posisi = 24;
			    	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			    	    }
			    	    else if( posisi == 98 ){
			    	        posisi = 3;
			   		        cout << "    Petak 98 ada Ular :( !!!" << endl;
			   		    }
			   	    	//bila melebihi jumlah petak
			   	    	else if( posisi > 100 ){
			        	    int sisa = posisi;
			        	    sisa -= 100;
			        	    posisi -= sisa;
			        	    posisi -= sisa;
			        	    if( posisi == 98 ){
			        	        posisi = 3;
			        	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			        	    }
			        	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = " << posisi4 << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
	    		else if(posisi == 0 && posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	        
	    	    }
	    	    else if(posisi3 == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else if(posisi == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
	   				cout << "             - " << pemain4 << " = " << posisi4 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain2 << endl;
	    	    if(posisi2 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi2 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi2 += dadu();
		   	    //posisi menang
		   	    if( posisi2 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain2 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi2 == 2 ){
		   	        posisi2 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 7 ){
		   	        posisi2 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 19 ){
		   	        posisi2 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 36 ){
		   	        posisi2 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi2 == 74 ){
		   	        posisi2 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi2 == 42 ){
		   	        posisi2 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 71 ){
		   	        posisi2 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 77 ){
		   	        posisi2 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi2 == 86 ){
		   	        posisi2 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi2 == 98 ){
		   	        posisi2 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi2 > 100 ){
		       	    int sisa = posisi2;
		       	    sisa -= 100;
		       	    posisi2 -= sisa;
		       	    posisi2 -= sisa;
		       	    if( posisi2 == 98 ){
		       	        posisi2 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi2 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi2 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi2 += dadu();
			   	    //posisi menang
			   	    if( posisi2 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain2 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	    	getch();
					    break;
			   	    }
			   	    //tangga
			   	    else if( posisi2 == 2 ){
			   	        posisi2 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 7 ){
			   	        posisi2 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 19 ){
			   	        posisi2 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 36 ){
			   	        posisi2 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi2 == 74 ){
			   	        posisi2 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi2 == 42 ){
			   	        posisi2 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 71 ){
			   	        posisi2 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 77 ){
			   	        posisi2 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi2 == 86 ){
			   	        posisi2 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi2 == 98 ){
			   	        posisi2 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi2 > 100 ){
			       	    int sisa = posisi2;
			       	    sisa -= 100;
			       	    posisi2 -= sisa;
			       	    posisi2 -= sisa;
			       	    if( posisi2 == 98 ){
			       	        posisi2 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi2 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi2 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi2 += dadu();
				   	    //posisi menang
				   	    if( posisi2 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi2 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain2 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi2 == 2 ){
				   	        posisi2 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 7 ){
				   	        posisi2 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 19 ){
				   	        posisi2 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 36 ){
				   	        posisi2 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi2 == 74 ){
				   	        posisi2 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi2 == 42 ){
				   	        posisi2 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 71 ){
				   	        posisi2 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 77 ){
				   	        posisi2 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi2 == 86 ){
				   	        posisi2 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi2 == 98 ){
				   	        posisi2 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi2 > 100 ){
				       	    int sisa = posisi2;
				       	    sisa -= 100;
				       	    posisi2 -= sisa;
				       	    posisi2 -= sisa;
				       	    if( posisi2 == 98 ){
				       	        posisi2 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi2 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi2 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi2 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = " << posisi4 << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
	    		else if(posisi == 0 && posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	        
	    	    }
	    	    else if(posisi3 == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else if(posisi == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
	   				cout << "             - " << pemain4 << " = " << posisi4 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain3 << endl;
	    	    if(posisi3 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi3 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi3 += dadu();
		   	    //posisi menang
		   	    if( posisi3 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain3 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi3 == 2 ){
		   	        posisi3 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 7 ){
		   	        posisi3 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 19 ){
		   	        posisi3 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 36 ){
		   	        posisi3 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi3 == 74 ){
		   	        posisi3 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi3 == 42 ){
		   	        posisi3 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 71 ){
		   	        posisi3 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 77 ){
		   	        posisi3 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi3 == 86 ){
		   	        posisi3 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi3 == 98 ){
		   	        posisi3 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi3 > 100 ){
		       	    int sisa = posisi3;
		       	    sisa -= 100;
		       	    posisi3 -= sisa;
		       	    posisi3 -= sisa;
		       	    if( posisi3 == 98 ){
		       	        posisi3 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi3 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi3 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi3 += dadu();
			   	    //posisi menang
			   	    if( posisi3 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain3 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	        getch();
						break;
			   	    }
			   	    //tangga
			   	    else if( posisi3 == 2 ){
			   	        posisi3 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 7 ){
			   	        posisi3 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 19 ){
			   	        posisi3 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 36 ){
			   	        posisi3 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi3 == 74 ){
			   	        posisi3 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi3 == 42 ){
			   	        posisi3 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 71 ){
			   	        posisi3 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 77 ){
			   	        posisi3 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi3 == 86 ){
			   	        posisi3 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi3 == 98 ){
			   	        posisi3 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi3 > 100 ){
			       	    int sisa = posisi3;
			       	    sisa -= 100;
			       	    posisi3 -= sisa;
			       	    posisi3 -= sisa;
			       	    if( posisi3 == 98 ){
			       	        posisi3 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi3 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi3 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi3 += dadu();
				   	    //posisi menang
				   	    if( posisi3 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi3 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain3 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi3 == 2 ){
				   	        posisi3 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 7 ){
				   	        posisi3 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 19 ){
				   	        posisi3 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 36 ){
				   	        posisi3 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi3 == 74 ){
				   	        posisi3 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi3 == 42 ){
				   	        posisi3 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 71 ){
				   	        posisi3 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 77 ){
				   	        posisi3 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi3 == 86 ){
				   	        posisi3 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi3 == 98 ){
				   	        posisi3 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi3 > 100 ){
				       	    int sisa = posisi3;
				       	    sisa -= 100;
				       	    posisi3 -= sisa;
				       	    posisi3 -= sisa;
				       	    if( posisi3 == 98 ){
				       	        posisi3 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi3 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi3 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi3 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
		        
		        system("cls");
				cout << "<<<<<<<<SNAKE TANGGA>>>>>>>>" << endl;
				cout << "       \"Beta Version\"       " << endl;
	    		cout << endl;
	   			cout << "Peta : " << endl;
	   			cout << "  - Ular   = 42 -> 5, " << endl;
    			cout << "             71 -> 33," << endl;
    			cout << "             77 -> 56," << endl;
    			cout << "             86 -> 24," << endl;
    			cout << "             98 -> 3  " << endl;
    			cout << endl;
    			cout << "  - Tangga =  2 -> 38," << endl;
    			cout << "              7 -> 53," << endl;
    			cout << "             19 -> 80," << endl;
    			cout << "             36 -> 66," << endl;
    			cout << "             74 -> 93 " << endl;
    			cout << endl;
    			if(posisi == 0 && posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = " << posisi4 << endl;
    			}
    			else if(posisi == 0 && posisi2 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = " << posisi3 << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
					cout << "             - " << pemain2 << " = " << posisi2 << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
    			else if(posisi2 == 0 && posisi3 == 0 && posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
					cout << "             - " << pemain2 << " = START" << endl;
					cout << "             - " << pemain3 << " = START" << endl;
					cout << "             - " << pemain4 << " = START" << endl;
    			}
	    		else if(posisi == 0 && posisi2 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	        
	    	    }
	    	    else if(posisi3 == 0 && posisi4 == 0){
	    	        cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else if(posisi == 0){
					cout << "  - Posisi : - " << pemain1 << " = START" << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi2 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = START" << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi3 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = START" << endl;
	    	        cout << "             - " << pemain4 << " = " << posisi4 << endl;
	    	    }
	    	    else if(posisi4 == 0){
					cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	    	        cout << "             - " << pemain2 << " = " << posisi2 << endl;
	    	        cout << "             - " << pemain3 << " = " << posisi3 << endl;
	    	        cout << "             - " << pemain4 << " = START" << endl;
	    	    }
	    	    else {
	   				cout << "  - Posisi : - " << pemain1 << " = " << posisi << endl;
	   				cout << "             - " << pemain2 << " = " << posisi2 << endl;
	   				cout << "             - " << pemain3 << " = " << posisi3 << endl;
	   				cout << "             - " << pemain4 << " = " << posisi4 << endl;
				}
				cout << endl;
	    	    cout << "Main : " << endl;
	    	    cout << "  - " << pemain4 << endl;
	    	    if(posisi4 == 0){
	    	        cout << "  - Posisi : START " << endl;
	    	    }
	    	    else {
	    	        cout << "  - Posisi : " << posisi4 << endl;
	    	    }
		   	    cout << "  - Roll Dadu : " << endl;
		   	    getch();
		   	    cout << "  - Dadu : " << dadu() << endl;
		   	    posisi4 += dadu();
		   	    //posisi menang
		   	    if( posisi4 == 100 ){
					system("Color 4F");
	    	        cout << "  - Posisi sekarang  : " << posisi4 << endl;
	    	        cout << endl;
	    	        cout << "Pemain : " << pemain4 << endl;
	    	        
	    	        system("Color 0A"); delay(50); cout << "||      ||  ";
	    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
	    	        system("Color 4E"); delay(50); cout << "||       ||  ";
	    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
	    	        system("Color 7B"); delay(50); cout << "||       ||  ";
	    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
	    	        system("Color 0E"); delay(50); cout << "||       ||  ";
	    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 2A"); delay(50); cout << "    ||      ";
	    	        system("Color 3B"); delay(50); cout << "||       ||  ";
	    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
	    	        
	    	        system("Color 5D"); delay(50); cout << "    ||      ";
	    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
	    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
	    	        cout << endl;
	    	        system("Color A0"); delay(50); cout << "||          ||  ";
	    	        system("Color B1"); delay(50); cout << "||||||  ";
	    	        system("Color C2"); delay(50); cout << "||||     ||  ";
	    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
	    			
	    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F5"); delay(50); cout << "  ||    ";
	    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
	    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
	    	        
	    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color D9"); delay(50); cout << "  ||    ";
	    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
	    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color B3"); delay(50); cout << "  ||    ";
	    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
	    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
	    	         
	    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
	    	        system("Color F7"); delay(50); cout << "  ||    ";
	    	        system("Color A8"); delay(50); cout << "||    || ||  ";
	    	        system("Color B9"); delay(50); cout << "        " << endl;
	    	         
	    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
	    	        system("Color D1"); delay(50); cout << " ||||||  ";
	    	        system("Color E2"); delay(50); cout << "||     ||||  ";
	    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
	    	        system("Color 4F");
		   	        getch();
					break;
		   	    }
		   	    //tangga
		   	    else if( posisi4 == 2 ){
		   	        posisi4 = 38;
		   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi4 == 7 ){
		   	        posisi4 = 53;
		   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi4 == 19 ){
		   	        posisi4 = 80;
		   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi4 == 36 ){
		   	        posisi4 = 66;
		   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
		   	    }
		   	    else if( posisi4 == 74 ){
		   	        posisi4 = 93;
		   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
		   	    }
		   	    //ular
		   	    else if( posisi4 == 42 ){
		   	        posisi4 = 5;
		   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi4 == 71 ){
		   	        posisi4 = 33;
		   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi4 == 77 ){
		   	        posisi4 = 56;
		   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
		   	    }
		   	    else if( posisi4 == 86 ){
		   	        posisi4 = 24;
		   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
		   	    }
	    	    else if( posisi4 == 98 ){
		   	        posisi4 = 3;
			        cout << "    Petak 98 ada Ular :( !!!" << endl;
			    }
		       	//bila melebihi jumlah petak
	 	    	else if( posisi4 > 100 ){
		       	    int sisa = posisi4;
		       	    sisa -= 100;
		       	    posisi4 -= sisa;
		       	    posisi4 -= sisa;
		       	    if( posisi4 == 98 ){
		       	        posisi4 = 3;
		       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
		       	    }
		       	}
		   	    //angka dadu 6 pertama
		   	    if( dadu() == 6 ){
		   	        cout << "    Anda roll dadu lagi :)" << endl;
		   	        if(posisi4 == 0){
		    	        cout << "  - Posisi : START " << endl;
		    	    }
		    	    else {
		    	        cout << "  - Posisi : " << posisi4 << endl;
		    	    }
			   	    cout << "  - Roll Dadu : " << endl;
			   	    getch();
			   	    cout << "  - Dadu : " << dadu() << endl;
			   	    posisi4 += dadu();
			   	    //posisi menang
			   	    if( posisi4 == 100 ){
						system("Color 4F");
		    	        cout << "  - Posisi sekarang  : " << posisi4 << endl;
		    	        cout << endl;
		    	        cout << "Pemain : " << pemain4 << endl;
		    	        
		    	        system("Color 0A"); delay(50); cout << "||      ||  ";
		    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
		    	        system("Color 4E"); delay(50); cout << "||       ||  ";
		    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
		    	        system("Color 7B"); delay(50); cout << "||       ||  ";
		    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
		    	        system("Color 0E"); delay(50); cout << "||       ||  ";
		    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 2A"); delay(50); cout << "    ||      ";
		    	        system("Color 3B"); delay(50); cout << "||       ||  ";
		    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
		    	        
		    	        system("Color 5D"); delay(50); cout << "    ||      ";
		    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
		    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
		    	        cout << endl;
		    	        system("Color A0"); delay(50); cout << "||          ||  ";
		    	        system("Color B1"); delay(50); cout << "||||||  ";
		    	        system("Color C2"); delay(50); cout << "||||     ||  ";
		    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
		    			
		    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F5"); delay(50); cout << "  ||    ";
		    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
		    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
		    	        
		    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color D9"); delay(50); cout << "  ||    ";
		    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
		    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color B3"); delay(50); cout << "  ||    ";
		    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
		    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
		    	         
		    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
		    	        system("Color F7"); delay(50); cout << "  ||    ";
		    	        system("Color A8"); delay(50); cout << "||    || ||  ";
		    	        system("Color B9"); delay(50); cout << "        " << endl;
		    	         
		    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
		    	        system("Color D1"); delay(50); cout << " ||||||  ";
		    	        system("Color E2"); delay(50); cout << "||     ||||  ";
		    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
		    	        system("Color 4F");
			   	        getch();
						break;
			   	    }
			   	    //tangga
			   	    else if( posisi4 == 2 ){
			   	        posisi4 = 38;
			   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi4 == 7 ){
			   	        posisi4 = 53;
			   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi4 == 19 ){
			   	        posisi4 = 80;
			   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi4 == 36 ){
			   	        posisi4 = 66;
			   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
			   	    }
			   	    else if( posisi4 == 74 ){
			   	        posisi4 = 93;
			   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
			   	    }
			   	    //ular
			   	    else if( posisi4 == 42 ){
			   	        posisi4 = 5;
			   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi4 == 71 ){
			   	        posisi4 = 33;
			   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi4 == 77 ){
			   	        posisi4 = 56;
			   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
			   	    }
			   	    else if( posisi4 == 86 ){
			   	        posisi4 = 24;
			   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
			   	    }
		    	    else if( posisi4 == 98 ){
			   	        posisi4 = 3;
				        cout << "    Petak 98 ada Ular :( !!!" << endl;
				    }
			       	//bila melebihi jumlah petak
		 	    	else if( posisi4 > 100 ){
			       	    int sisa = posisi4;
			       	    sisa -= 100;
			       	    posisi4 -= sisa;
			       	    posisi4 -= sisa;
			       	    if( posisi4 == 98 ){
			       	        posisi4 = 3;
			       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
			       	    }
			       	}
		        	//angka dadu 6 kedua
		      		if( dadu() == 6 ){
	 	    	    	cout << "    Anda main lagi :)" << endl;
		       	        if(posisi4 == 0){
			    	        cout << "  - Posisi : START " << endl;
			    	    }
			    	    else {
			    	        cout << "  - Posisi : " << posisi4 << endl;
			    	    }
				   	    cout << "  - Roll Dadu : " << endl;
				   	    getch();
				   	    cout << "  - Dadu : " << dadu() << endl;
				   	    posisi4 += dadu();
				   	    //posisi menang
				   	    if( posisi4 == 100 ){
							system("Color 4F");
			    	        cout << "  - Posisi sekarang  : " << posisi4 << endl;
			    	        cout << endl;
			    	        cout << "Pemain : " << pemain4 << endl;
			    	        
			    	        system("Color 0A"); delay(50); cout << "||      ||  ";
			    	        system("Color 1B"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 2C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 3D"); delay(50); cout << " ||    ||   ";
			    	        system("Color 4E"); delay(50); cout << "||       ||  ";
			    	        system("Color 5F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 6A"); delay(50); cout << "  ||\\/||    ";
			    	        system("Color 7B"); delay(50); cout << "||       ||  ";
			    	        system("Color 8C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 9D"); delay(50); cout << "   \\||/     ";
			    	        system("Color 0E"); delay(50); cout << "||       ||  ";
			    	        system("Color 1F"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 2A"); delay(50); cout << "    ||      ";
			    	        system("Color 3B"); delay(50); cout << "||       ||  ";
			    	        system("Color 4C"); delay(50); cout << "||      ||" << endl;
			    	        
			    	        system("Color 5D"); delay(50); cout << "    ||      ";
			    	        system("Color 6E"); delay(50); cout << "|||||||||||  ";
			    	        system("Color 7F"); delay(50); cout << "||||||||||" << endl;
			    	        cout << endl;
			    	        system("Color A0"); delay(50); cout << "||          ||  ";
			    	        system("Color B1"); delay(50); cout << "||||||  ";
			    	        system("Color C2"); delay(50); cout << "||||     ||  ";
			    	        system("Color D3"); delay(50); cout << "|| || ||" << endl;
			    			
			    	        system("Color E4"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F5"); delay(50); cout << "  ||    ";
			    	        system("Color A6"); delay(50); cout << "|| ||    ||  ";
			    	        system("Color B7"); delay(50); cout << "|| || ||" << endl;
			    	        
			    	        system("Color C8"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color D9"); delay(50); cout << "  ||    ";
			    	        system("Color E0"); delay(50); cout << "||  ||   ||  ";
			    	        system("Color F1"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color A2"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color B3"); delay(50); cout << "  ||    ";
			    	        system("Color C4"); delay(50); cout << "||   ||  ||  ";
			    	        system("Color D5"); delay(50); cout << "|| || ||" << endl;
			    	         
			    	        system("Color E6"); delay(50); cout << "||    ||    ||  ";
			    	        system("Color F7"); delay(50); cout << "  ||    ";
			    	        system("Color A8"); delay(50); cout << "||    || ||  ";
			    	        system("Color B9"); delay(50); cout << "        " << endl;
			    	         
			    	        system("Color C0"); delay(50); cout << " \\][][/\\][][/  ";
			    	        system("Color D1"); delay(50); cout << " ||||||  ";
			    	        system("Color E2"); delay(50); cout << "||     ||||  ";
			    	        system("Color F3"); delay(50); cout << "() () ()" << endl;
			    	        system("Color 4F");
				   	        getch();
							break;
				   	    }
				   	    //tangga
				   	    else if( posisi4 == 2 ){
				   	        posisi4 = 38;
				   	        cout << "    Petak 2 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi4 == 7 ){
				   	        posisi4 = 53;
				   	        cout << "    Petak 7 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi4 == 19 ){
				   	        posisi4 = 80;
				   	        cout << "    Petak 19 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi4 == 36 ){
				   	        posisi4 = 66;
				   	        cout << "    Petak 36 ada Tangga :) !!!" << endl;
				   	    }
				   	    else if( posisi4 == 74 ){
				   	        posisi4 = 93;
				   	        cout << "    Petak 74 ada Tangga :) !!!" << endl;
				   	    }
				   	    //ular
				   	    else if( posisi4 == 42 ){
				   	        posisi4 = 5;
				   	        cout << "    Petak 42 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi4 == 71 ){
				   	        posisi4 = 33;
				   	        cout << "    Petak 71 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi4 == 77 ){
				   	        posisi4 = 56;
				   	        cout << "    Petak 77 ada Ular :( !!!" << endl;
				   	    }
				   	    else if( posisi4 == 86 ){
				   	        posisi4 = 24;
				   	        cout << "    Petak 86 ada Ular :( !!!" << endl;
				   	    }
			    	    else if( posisi4 == 98 ){
				   	        posisi4 = 3;
					        cout << "    Petak 98 ada Ular :( !!!" << endl;
					    }
				       	//bila melebihi jumlah petak
			 	    	else if( posisi4 > 100 ){
				       	    int sisa = posisi4;
				       	    sisa -= 100;
				       	    posisi4 -= sisa;
				       	    posisi4 -= sisa;
				       	    if( posisi4 == 98 ){
				       	        posisi4 = 3;
				       	        cout << "    Petak 98 ada Ular :( !!!" << endl;
				       	    }
				       	}
		                //angka dadu 6 ketiga
		                if( dadu() == 6 ){
		                    cout << "    Anda sudah 3x angka 6" << endl;
		                    posisi3 = 0;
		                }
		           }
		        }
		        //posisi ketika angka dadunya 6 untuk ke 3x
		        if(posisi4 == 0){
		            cout << "  - Posisi sekarang  : START " << endl;
		        }
		        //posisi setelah lempar dadu
		        else {
		            cout << "  - Posisi sekarang  : " << posisi4 << endl;
		        }
		        cout << "  - Selesai ( y ) ";
		        getch();
	        }while( true );
   		}
   		else{
			cout << endl;
			cout << "Jumlah Pemain yang anda masukan belum tersedia" << endl;
			cout << "Tunggulah Update selanjutnya" << endl;
			cout << ":)";
			getch();
   		}
	}
    else{
        cout << "Mmmm Oke, Bye ! " << endl;
        cout << "(-_- )//";
        getch();
    }
}
